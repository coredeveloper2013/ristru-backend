@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
           <div class="card-danger">
                <div class="left">Aggiorna progetto</div>
                <div class="right">
                    <i class="zmdi zmdi-edit"></i>
                </div>
            </div>
            <div id="card-body" class="card-body">
                <!-- start fomr -->
                <form action="{{ route('profile.update') }}" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $profile->id }}">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label for="title_it" class="control-label"><span class="text-danger">*</span>Title (Italiano)</label>
                            <input type="text" class="form-control" name="title_it" id="title_it" placeholder="Title" value="{{ $profile->title_it }}" required>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                            <label for="title_en" class="control-label">Title (English)</label>
                            <input type="text" class="form-control" name="title_en" id="title_en" value="{{ $profile->title_en }}" placeholder="Title">
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                            <label for="title_de" class="control-label">Title (Deutsch)</label>
                            <input type="text" class="form-control" name="title_de" id="title_de" value="{{ $profile->title_de }}" placeholder="Title">
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                            <label for="title_py" class="control-label">Title (Pусский)</label>
                            <input type="text" class="form-control" name="title_py" id="title_py" value="{{ $profile->title_py }}" placeholder="Title">
                        </div>
                      </div>
                    </div>
                    

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group has-feedback">
                            <label for="description_it" class="control-label"><span class="text-danger">*</span>Description (Italiano)</label>
                            <textarea class="form-control" rows="5" name="description_it" id="description_it" required>{{ $profile->description_it }}</textarea>
                            <div class="help-block with-errors">
                                
                            </div>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group has-feedback">
                            <label for="description_en" class="control-label">Description (English)</label>
                            <textarea class="form-control" rows="5" name="description_en" id="description_en">{{ $profile->description_en }}</textarea>
                            <div class="help-block with-errors">
                                
                            </div>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group has-feedback">
                            <label for="description_de" class="control-label">Description (Deutsch)</label>
                            <textarea class="form-control" rows="5" name="description_de" id="description_de" >{{ $profile->description_de }}</textarea>
                            <div class="help-block with-errors">
                                
                            </div>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group has-feedback">
                            <label for="description_py" class="control-label">Description (Pусский)</label>
                            <textarea class="form-control" rows="5" name="description_py" id="description_py">{{ $profile->description_py }}</textarea>
                            <div class="help-block with-errors">
                                
                            </div>
                        </div>
                      </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                <div id="file-upload-form-1" class="uploader">
                                    <input id="file-upload-1" type="file" name="image_before" accept="image/*" />

                                    <label for="file-upload-1" id="file-drag-1">
                                        <img id="file-image-1" height="300" width="300" src="{{ asset('public/storage/uploads/'.$profile->image_before)}}" alt="Preview" class="">
                                        <div id="start-1">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                        <div>Select a <b>Before Image</b> or drag here & maintain the ratio(294x274)</div>
                                        <div id="notimage-1" class="hidden">Please select an image</div>
                                        <span id="file-upload-btn-1" class="btn btn-primary">Select Before Image</span>
                                        </div>
                                        <div id="response-1" class="hidden">
                                        <div id="messages-1"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                       
                        
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                <div id="file-upload-form-2" class="uploader">
                                    <input id="file-upload-2" type="file" name="image_after" accept="image/*" />

                                    <label for="file-upload-2" id="file-drag-2">
                                        <img id="file-image-2" height="300" width="300" src="{{ asset('public/storage/uploads/'.$profile->image_after)}}" alt="Preview" class="">
                                        <div id="start-2">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                        <div>Select a <b>After Image</b> or drag here & maintain the ratio(294x274)</div>
                                        <div id="notimage-2" class="hidden">Please select an image</div>
                                        <span id="file-upload-btn-2" class="btn btn-primary">Select After Image</span>
                                        </div>
                                        <div id="response-2" class="hidden">
                                        <div id="messages-2"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="tipologia_it" class="control-label">Tipologia (Italiano)</label>
                                <input type="text" class="form-control" name="tipologia_it" id="tipologia_it" value="{{ $profile->tipologia_it }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="tipologia_en" class="control-label">Tipologia (English)</label>
                                <input type="text" class="form-control" name="tipologia_en" id="tipologia_en" value="{{ $profile->tipologia_en }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="tipologia_de" class="control-label">Tipologia (Deutsch)</label>
                                <input type="text" class="form-control" name="tipologia_de" id="tipologia_de" value="{{ $profile->tipologia_de }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="tipologia_py" class="control-label">Tipologia (Pусский)</label>
                                <input type="text" class="form-control" name="tipologia_py" id="tipologia_py" value="{{ $profile->tipologia_py }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="price_it" class="control-label">Budget (Italy)</label>
                                <input type="text" class="form-control" name="price_it" id="price_it" value="{{ $profile->price_it }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="price_en" class="control-label">Budget (English)</label>
                                <input type="text" class="form-control" name="price_en" id="price_en" value="{{ $profile->price_en }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="price_de" class="control-label">Budget (Deutsch)</label>
                                <input type="text" class="form-control" name="price_de" id="price_de" value="{{ $profile->price_de }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="price_py" class="control-label">Budget (Pусский)</label>
                                <input type="text" class="form-control" name="price_py" id="price_py" value="{{ $profile->price_py }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="location_it" class="control-label">Città (Italiano)</label>
                                <input type="text" class="form-control" name="location_it" id="location_it" value="{{ $profile->location_it }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="location_en" class="control-label">Città (English)</label>
                                <input type="text" class="form-control" name="location_en" id="location_en" value="{{ $profile->location_en }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="location_de" class="control-label">Città (Deutsch)</label>
                                <input type="text" class="form-control" name="location_de" id="location_de" value="{{ $profile->location_de }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="location_py" class="control-label">Città (Pусский)</label>
                                <input type="text" class="form-control" name="location_py" id="location_py"  value="{{ $profile->location_py }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="mq_it" class="control-label">Superficie (Italiano)</label>
                                <input type="text" class="form-control" name="mq_it" id="mq_it"  value="{{ $profile->mq_it }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="mq_en" class="control-label">Superficie (English)</label>
                                <input type="text" class="form-control" name="mq_en" id="mq_en"  value="{{ $profile->mq_en }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="mq_de" class="control-label">Superficie (Deutsch)</label>
                                <input type="text" class="form-control" name="mq_de" id="mq_de"  value="{{ $profile->mq_de }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="mq_py" class="control-label">Superficie (Pусский)</label>
                                <input type="text" class="form-control" name="mq_py" id="mq_py" value="{{ $profile->mq_py }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="network_it" class="control-label">Consegna (Italiano)</label>
                                <input type="text" class="form-control" name="network_it" id="network_it" value="{{ $profile->network_it }}">
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="network_en" class="control-label">Consegna (English)</label>
                                <input type="text" class="form-control" name="network_en" id="network_en" value="{{ $profile->network_en }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="network_de" class="control-label">Consegna (Deutsch)</label>
                                <input type="text" class="form-control" name="network_de" id="network_de" value="{{ $profile->network_de }}">
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-feedback">
                                <label for="network_py" class="control-label">Consegna (Pусский)</label>
                                <input type="text" class="form-control" name="network_py" id="network_py" value="{{ $profile->network_py }}">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <label for="sorting" class="control-label"><span class="text-danger">*</span>Sorted Position</label>
                                <input type="number" class="form-control" name="sorting" id="sorting" value="{{ $profile->sorting }}">
                                <div class="help-block with-errors">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="form">
                        <button type="submit" class="btn btn-primary">Aggiorna progetto</button>
                    </div>
                </form>
                    <!-- end form -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<style>
    .uploader {
      display: block;
      clear: both;
      margin: 0 auto;
      width: 100%;
      max-width: 600px;
    }
    #messages-1, #messages-2{
        padding: 15px;
    }
    .uploader label {
      float: left;
      clear: both;
      width: 100%;
      height: 420px;
      padding: 2rem 1.5rem;
      text-align: center;
      background: #fff;
      border-radius: 7px;
      border: 3px solid #eee;
      transition: all .2s ease;
      -webkit-user-select: none;
         -moz-user-select: none;
          -ms-user-select: none;
              user-select: none;
    }
    .uploader label:hover {
      border-color: #454cad;
    }
    .uploader label.hover {
      border: 3px solid #454cad;
      box-shadow: inset 0 0 0 6px #eee;
    }
    .uploader label.hover #start i.fa {
      -webkit-transform: scale(0.8);
              transform: scale(0.8);
      opacity: 0.3;
    }
    .uploader #start {
      float: left;
      clear: both;
      width: 100%;
    }
    .uploader #start.hidden {
      display: none;
    }
    .uploader #start i.fa {
      font-size: 50px;
      margin-bottom: 1rem;
      transition: all .2s ease-in-out;
    }
    .uploader #response {
      float: left;
      clear: both;
      width: 100%;
    }
    .uploader #response.hidden {
      display: none;
    }
    .uploader #response #messages {
      margin-bottom: .5rem;
    }
    .uploader #file-image {
      display: inline;
      margin: 0 auto .5rem auto;
      width: auto;
      height: auto;
      max-width: 180px;
    }
    .uploader #file-image.hidden {
      display: none;
    }
    .uploader #notimage {
      display: block;
      float: left;
      clear: both;
      width: 100%;
    }
    .uploader #notimage.hidden {
      display: none;
    }
    .uploader progress,
    .uploader .progress {
      display: inline;
      clear: both;
      margin: 0 auto;
      width: 100%;
      max-width: 180px;
      height: 8px;
      border: 0;
      border-radius: 4px;
      background-color: #eee;
      overflow: hidden;
    }
    .uploader .progress[value]::-webkit-progress-bar {
      border-radius: 4px;
      background-color: #eee;
    }
    .uploader .progress[value]::-webkit-progress-value {
      background: linear-gradient(to right, #393f90 0%, #454cad 50%);
      border-radius: 4px;
    }
    .uploader .progress[value]::-moz-progress-bar {
      background: linear-gradient(to right, #393f90 0%, #454cad 50%);
      border-radius: 4px;
    }
    .uploader input[type="file"] {
      display: none;
    }
    .uploader div {
      margin: 0 0 .5rem 0;
      color: #5f6982;
    }
    .uploader .btn {
      display: inline-block;
      margin: .5rem .5rem 1rem .5rem;
      clear: both;
      font-family: inherit;
      font-weight: 700;
      font-size: 14px;
      text-decoration: none;
      text-transform: initial;
      border: none;
      border-radius: .2rem;
      outline: none;
      padding: 0 1rem;
      height: 36px;
      line-height: 36px;
      color: #fff;
      transition: all 0.2s ease-in-out;
      box-sizing: border-box;
      background: #454cad;
      border-color: #454cad;
      cursor: pointer;
    }
</style>
@endpush

@push('js')
    <script>
        function ekUpload(){
        function Init() {

    console.log("Upload Initialised");

    var fileSelect1    = document.getElementById('file-upload-1'),
        fileSelect2    = document.getElementById('file-upload-2'),
        fileDrag1      = document.getElementById('file-drag-1'),
        fileDrag2      = document.getElementById('file-drag-2'),
        submitButton1  = document.getElementById('submit-button-1');
        submitButton2  = document.getElementById('submit-button-2');

    fileSelect1.addEventListener('change', fileSelectHandler1, false);
    fileSelect2.addEventListener('change', fileSelectHandler2, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag1.addEventListener('dragover', fileDragHover1, false);
      fileDrag2.addEventListener('dragover', fileDragHover2, false);
      fileDrag1.addEventListener('dragleave', fileDragHover1, false);
      fileDrag2.addEventListener('dragleave', fileDragHover2, false);
      fileDrag1.addEventListener('drop', fileSelectHandler1, false);
      fileDrag2.addEventListener('drop', fileSelectHandler2, false);
    }
  }

  function fileDragHover1(e) {
    var fileDrag = document.getElementById('file-drag-1');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileDragHover2(e) {
    var fileDrag = document.getElementById('file-drag-2');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler1(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover1(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile1(f);
      //uploadFile(f);
    }
  }

  function fileSelectHandler2(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover2(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile2(f);
      //uploadFile(f);
    }
  }

  // Output
  function output1(msg) {
    // Response
    var m = document.getElementById('messages-1');
    m.innerHTML = msg;
  }

  function output2(msg) {
    // Response
    var m = document.getElementById('messages-2');
    m.innerHTML = msg;
  }

  function parseFile1(file) {

    console.log(file.name);
    output1(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );
    
    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;
     console.log("Image1 " + imageName)

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start-1').classList.add("hidden");
      document.getElementById('response-1').classList.remove("hidden");
      document.getElementById('notimage-1').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image-1').classList.remove("hidden");
      document.getElementById('file-image-1').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image-1').classList.add("hidden");
      document.getElementById('notimage-1').classList.remove("hidden");
      document.getElementById('start-1').classList.remove("hidden");
      document.getElementById('response-1').classList.add("hidden");
      document.getElementById("file-upload-form-1").reset();
    }
  }

  function parseFile2(file) {

    console.log(file.name);
    output2(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );
    
    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;
    console.log("Image2 " + imageName)

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start-2').classList.add("hidden");
      document.getElementById('response-2').classList.remove("hidden");
      document.getElementById('notimage-2').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image-2').classList.remove("hidden");
      document.getElementById('file-image-2').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image-2').classList.add("hidden");
      document.getElementById('notimage-2').classList.remove("hidden");
      document.getElementById('start-2').classList.remove("hidden");
      document.getElementById('response-2').classList.add("hidden");
      document.getElementById("file-upload-form-2").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  function uploadFile(file) {

    var xhr = new XMLHttpRequest(),
      fileInput = document.getElementById('class-roster-file'),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 1024; // In MB
    if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);

        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!

            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
          }
        };

        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.send(file);
      } else {
        output2('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}
ekUpload();

    </script>
@endpush


<style>
  .nav-item{margin-top: 1px !important;}
</style>
<div class="sidebar">
  <h1 class="head">Navigation</h1>
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link {{ $active == 'profile' ? 'active':'' }}" href="{{ route('profile.show') }}">
        <i class="left zmdi zmdi-home"></i>Progetto
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link {{ $active == 'il-tuo-architetto' ? 'active':'' }}" href="{{ route('architect.email.show') }}">
        <i class="left zmdi zmdi-balance"></i>Il Tuo Architetto
      </a>
    </li>
  </ul>
</div>

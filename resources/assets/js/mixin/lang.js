let Language = {
    __: function (str) {
        let d = [
            // Top Bar
            {it: 'DOVE SIAMO', en:'WHERE WE ARE'},
            {it: 'Simone in tutta Italia', en:'Find your near Office !'},
            {it: 'Chiamaci adesso!', en:'CALL US NOW'},
            {it: "Mandaci un'email", en:'WRITE US AN EMAIL'},
            {it: 'AMBIENTI', en:'INTERIOR DESIGN'},
            {it: 'AMBIENTI', en:'INTERIOR DESIGN'},
            {it: 'I NOSTRI PROGETTI', en:'OUR PROJECTS'},
            {it: 'CHI SIAMO', en:'WHO WE ARE'},
            {it: 'CONTATTACI', en:'CONTACT US'},
            {it: 'IL TUO ARCHITETTO', en:'YOUR ARCHITECT'},

            // Banner
            {it: 'SIAMO LEADER IN ITALIA', en:'WE ARE LEADER IN ITALY'},
            {it: 'Ristruttura la tua casa', en:'Renew your House'},
            {it: 'con noi!', en:'with us!'},
            {it: 'Cosa facciamo', en:'WHAT WE DO'},
            {it: 'CUCINA', en:''},

            // form 1
            {it: 'Inizia oggi la tua ristrutturazione', en:'Start today your renovation'},
            {it: 'Scrivi il tuo nome', en:'Type here your name'},
            {it: 'la tua email', en:'Your email'},
            {it: 'Cellulare', en:'Mobile phone'},
            {it: 'Cosa vuoi ristrutturare', en:'Choose the room'},
            {it: 'Inviando il form autorizzi Ristrutturazione case a contattarti per fini commerciali. Dichiari inoltre di aver letto e di accettare la Privacy Policy.', en:'By submitting this form you authorize Ristrutturazione Case to contacts for commercial purposes. You also declare that you have read and accept the Privacy Policy.'},
            {it: 'Completa la richiesta', en:'Complete the request'},
            {it: 'PREVENTIVO GRATUITO', en:'FREE QUOTE'},

            // Restructure
            {it: 'LA RISTRUTTURAZIONE CHE LA TUA CASA VUOLE', en:'THE RENOVATION THAT YOUR HOUSE WANTS.'},
            {it: 'Ristruttura oggi la tua casa!', en:'Renew today you House!'},
            {it: 'Progetto virtuale VR', en:'A VR dedicated project'},
            {it: 'Un progetto in realtà virtuale cosi da avere la certezza di come sarà l’appartamento ristrutturato ancor prima di iniziare i lavori.', en:'A virtual reality project so as to be sure of how the apartment will be renovated before starting work.'},
            {it: 'Un unico Architetto', en:'One architect for you'},
            {it: 'unico Architetto', en:'Architect for you'},
            {it: 'Uno dei nostri architetti come unico referente dei lavori, che segue il processo di ristrutturazione dal progetto alla consegna chiavi in mano.', en:'One of our architects as the sole referent of the works, which follows the restructuring process from the project to turnkey delivery.'},
            {it: 'Appartamento gratuito', en:'Free flat'},
            {it: 'Somontaggio mobili', en:'Furniture replacement'},
            {it: 'Smontaggio, fermo deposito e rimontaggio dei tuoi mobili a fine ristrutturazione; il tutto a costo zero. Un servizio efficiente con tempi brevi e certi.', en:'Dismantling, storage and reassembly of your furniture at the end of the renovation; all at no cost. An efficient service with short and certain times.'},
            {it: 'Applicazione dedicata', en:'Mobile application'},
            {it: 'Controlla l’avanzamento dei lavori grazie ad un’applicazione dedicata e scopri come stiamo effettuando la tua ristrutturazione.', en:'Check the progress of the work thanks to a dedicated application and find out how we are doing your renovation.'},
            {it: 'Rispetto dei tempi', en:'Respected dedaline'},
            {it: 'Rispetto dei tempi', en:'Respected dedaline'},
            {it: 'Uno dei nostri architetti come unico referente dei lavori, che segue il processo di ristrutturazione dal progetto alla consegna chiavi iRispetto dei tempi di consegna della tua casa ristrutturata. Intraprendi la tua ristrutturazione con tranquillità, garantiamo tempi certi di consegna.', en:'One of our architects as the sole referent of the works, which follows the restructuring process from the project to the delivery of the keys in respect of the delivery times of your renovated home. Undertake your restructuring with peace of mind, we guarantee certain delivery times.'},
            {it: 'Progetto virtuale VR', en:'A VR dedicated project'},
            {it: 'Progetto virtuale', en:'A dedicated project'},

            // Renovations
            {it: 'PROGETTI RECENTI', en:'RECENT RENOVATION PROJECTS'},
            {it: 'Nuovi spazi da scroprire', en:'New space to discover'},

            // Quote
            {it: 'Richiedi un preventivo gratuito', en:'Request a free quote!'},
            {it: 'CHIAMACI AL 800 33 33 30', en:'CALL US 800 33 33 30'},
            {it: 'CHIAMACI AL 800 33 33 30', en:'CALL US 800 33 33 30'},
            {it: 'PRENOTA UN APPUNTAMENTO', en:'BOOK TODAY YOUR APPOINTMENT'},
            {it: 'Matteo di Maglie', en:''},

            // Footer
            {it: 'Menu veloce', en:'Quick menu'},
            {it: 'Homepage', en:'Homepage'},
            {it: 'Preventivo gratuito', en:'Free quote'},
            {it: 'Contattaci', en:'Contact us'},
            {it: 'Siamo in tutta Italia', en:'We cover all Itlay'},
            {it: 'Tutti i testi e la grafica presenti nel sito sono soggetti alle norme vigenti sul diritto d autore.', en:'All texts and graphics on this site are subject to the current copyright regulations.'},
            {it: 'Segnala un problema', en:'Feedback a problem'},

            // banner all
            {it: 'RISTRUTTURAZIONE CHIAVI IN MANO', en:'KEY RESTRUCTURING IN HAND'},
            {it: 'Ristrutturazione Cucine', en:'Kitchen Renovation'},
            {it: 'CUCINA', en:'KITCHEN'},

            // Architect Renovations
            {it: 'Responsabile produzione', en:'Production manager'},
            {it: 'Direzione', en:'Direction'},
            {it: 'Amministrazione', en:'Administration'},
            {it: 'Architetti', en:'Architects'},
            

            // single Architect
            {it: 'spanLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed scelerisque libero sed tincidunt elementum. Praesent elementum, risus nec dictum consectetur, turpis enim semper odio, ut efficitur tellus justo nec odio. Integer interdum justo at ante lacinia, a molestie ex eleifend. Maecenas pharetra convallis elementum. Praesent pharetra ante vitae est pharetra, id aliquam sem tempus. Mauris et nisi porta, hendrerit purus vel, ullamcorper enim. Nulla quis urna ut nisi vehicula fringilla at finibus urna. Nunc convallis mi eu porttitor tristique.', en: ''},
            {it: 'Cras placerat sit amet ex quis cursus. Sed at est sit amet urna condimentum iaculis. Mauris et purus ut massa vehicula convallis vitae pharetra magna.', en: ''},
            {it: 'LA MIA ESPERIENZA', en:''},
            {it: 'I MIEI PUNTI FORTI', en:''},
            {it: 'Ristrutturazione Case', en:'Ristrutturazione Case'},
            {it: 'Dal 2013 ad oggi', en:''},
            {it: 'Società xyz', en:''},
            {it: 'Dal', en:''},
            {it: 'al', en:''},
            {it: 'Politecnico di Milano', en:''},

            // Ambienti
            {it: 'SONO ESPERTO IN ARREDAMENTO CONTEMPORANEO', en:''},
            {it: 'Scegli il tuo ambiente', en:''},
            {it: 'APPARTAMENTO', en:'FLAT'},
            {it: 'NEGOZIO', en:'SHOP'},
            {it: 'UFFICIO', en:'OFFICE'},
            {it: 'Un progetto in realtà', en:'A project in reality'},
            {it: 'virtuale cosi da avere', en:'virtual so as to be'},
            {it: 'LAVORI DI RISTUTTURAZIONE RECENTI', en:''},
            {it: 'Ambienti nuovi da scoprire', en:''},
            {it: 'SOGGIORNO', en:''},
            {it: 'Ristruttrazione appartamento 90mq', en:''},
            {it: 'Scopri di più', en:''},
            {it: 'SOGGIORNO', en:''},

            // portfolio
            {it: 'Le nostre ristrutturazioni', en:''},
            {it: 'Cucina shabby shic', en:''},
            {it: 'SCOPRI DI PIU', en:'FIND OUT MORE'},
            
            // single portfolio
            {it: 'Vivamus tempus risus metus, sed porttitor nibh auctor et. Nunc hendrerit diam sed turpis imperdiet, ut lobortis dolor bibendum. Sed gravida pretium leo in suscipit. Vestibulum pharetra tortor tellus, ut mattis magna fringilla sit amet. Fusce iaculis nisi quis ex dapibus, non pellentesque ipsum pulvinar. Suspendisse euismod quis erat a lobortis. Sed pellentesque pellentesque erat vitae luctus. Cras quis est enim. Maecenas quam lacus, eleifend vel leo eget, malesuada porta lorem. Nam lectus libero, congue id facilisis et, molestie id risus. Morbi volutpat, odio nec commodo egestas, risus ligula viverra ante, ac dictum eros ante in augue. Suspendisse justo massa, maximus nec erat a, ultricies tincidunt ante.', en:''},
            {it: 'Ut vehicula consequat nulla, sit amet aliquam lorem luctus et. Phasellus consectetur congue lacinia. Morbi tincidunt nunc ut libero aliquam, eget accumsan ante rutrum. Donec auctor faucibus leo, eu venenatis leo aliquam in. Sed at tellus in leo porttitor tempor vitae sagittis lacus. Sed a aliquam massa. Morbi sed dui eu ante commodo finibus vel id ante. Mauris vitae odio libero. Praesent pellentesque, velit sed elementum porta, neque orci mattis augue, sed mattis arcu turpis a ipsum. Quisque condimentum, neque eu sagittis sodales, sapien lorem iaculis dolor, sed rutrum nibh sem convallis orci.', en:''},
            {it: 'SMONTAGGIO MOBILI', en:''},
            {it: 'COMPILA IL FORM', en:''},
            {it: 'Cliente Privato', en:''},
            {it: 'Tipologia', en:''},
            {it: 'Data', en:'Date'},
            {it: 'Budget', en:'Budget'},
            {it: 'Città', en:'City'},
            {it: 'Superficie', en:'Surface'},
            {it: 'Consegna', en:'Delivery'},
            {it: 'Condividi', en:'Share'},
            {it: '12 giorni', en:'12 days'},
            

            // contact
            {it: 'Siamo in tutta italia!', en:''},
            {it: 'Via Petrocchi', en:''},
            {it: 'Milano', en:''},

            // about 
            {it: 'Chiedici un preventivo gratuito!', en: 'Ask us for a free quote!'},
            {it: 'PRENOTA OGGI UN APPUNTAMENTO', en: ''},
            {it: 'commerciali. Dichiari inoltre di aver letto e di accettare la Privacy Policy.', en: ''},
            {it: 'Inviando questo form autorizzi Ristrutturazione Case a contatti per fini', en: ''},
            {it: 'grazie. Adesso le richiedo informazioni più dettagliete sulla ristrutturazione.', en: ''},
            {it: 'L’appuntamento telefonico ha scopo di analizzare il progetto, la fattibilità, costi e tempistiche generali non vincolanti.', en: ''},
            {it: 'Desidero il sopralluogo', en: ''},
            {it: 'Un Architetto incaricato verrà a', en: ''},
            {it: 'isionare l’appartamento o', en: ''},
            {it: 'il negozio', en: ''},
            {it: 'Se si, indicami la città', en: ''},
            {it: 'grazie. Abbiamo preso in carico la tua richiesta di preventivo. Un incaricato si metterà in contatto con te. Abbiamo anche provveduto ad invitarti una copia della richiesta sulla tua casella email.', en: ''},
            {it: 'Preventivo numero:', en: ''},


        ];

        if(localStorage.lang !== undefined){
            this.$store.state.lang = localStorage.lang;
        }
        else {
            localStorage.lang = this.$store.state.lang;
        }
        let lang = this.$store.state.lang;
        let index = d.findIndex((item) => item.it.toLowerCase() === str.toLowerCase());
        if(index > -1){
            if(d[index][lang] !== undefined){
                return d[index][lang];
            }
            else {
                return str;
            }
        } else {
            return str;
        }
    }
};

module.exports = Language;
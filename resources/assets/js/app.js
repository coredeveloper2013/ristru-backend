import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import {routes} from './routes/router.js'

// Plugin
// ==================
import axios from 'axios'
import VueAxios from 'vue-axios'
import {store} from './store/store'


// window.$ = window.jQuery = require('jquery');

Vue.use(VueAxios, axios);
Vue.use(VueRouter);


import Language from './mixin/lang';
// Vue.mixin({methods : Language});
const Mixins = Object.assign({}, Language);
Vue.mixin({
    methods : Mixins
});

const router = new VueRouter({
    mode: 'history',
    routes: routes
}); 

global.core = {
    // APP: 'https://whatznext2018.000webhostapp.com/public',
    APP: 'http://localhost:8080',
    // Asset_Path: 'http://localhost/projects/mediusware/ristru-assets/',
    Asset_Path: 'https://dev2.simonechinaglia.net/dev/ristru/',
    // API: 'http://52.221.204.156/whats-next/public/index.php/en/api/v0.1'
};
import 'owl.carousel'
import { MdField,MdMenu,MdList } from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css';

Vue.use(MdField);
Vue.use(MdMenu);
Vue.use(MdList);

new Vue({
    el: '#app',
    store: store,
    router: router,
    render: h => h(App),
    created() {
         let script = document.createElement('script');
         script.src = 'https://maps.googleapis.com/maps/api/js';
         document.body.appendChild(script);
    },
});

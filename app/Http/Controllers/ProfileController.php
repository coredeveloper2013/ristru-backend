<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\EmailGeneratePdf;

use App\Mail\EmailGenerator;
use Illuminate\Support\Facades\Mail;



class ProfileController extends Controller
{
    public function all()
    {
        $profiles = Profile::orderBy('sorting', 'asc')->get();
        return response()->json([
            "profiles" => $profiles
        ], 200);
    }

    public function projects()
    {
        $projects = Profile::orderBy('sorting', 'asc')->limit(3)->get();
        return response()->json([
            "projects" => $projects
        ], 200);
    }

    public function get($title, $id)
    {
        $title = str_replace("-", ' ', $title);
        $profile = Profile::where('id', $id)->first();
        return response()->json([
            "profile" => $profile
        ], 200);
    }

    public function postSendEmail(Request $request){

        $name = $request->input('name');
        $room = $request->input('room');
        $date = $request->input('date');
        $phone = $request->input('phone');
        $flag = $request->input('flag');
        $from_email = $request->input('email');
        $want_architect = $request->input('want_architect');
        $filename = "";
        if($request->hasFile('file')){
            $ext = $request->file('file')->getClientOriginalExtension();
            $filename = $request->file('file')->getClientOriginalName();

            $upload = $request->file('file')->storeAs(
                'public/uploads', $filename
            );

        }

        $emailGeneratedPdf = new EmailGeneratePdf();
        $emailGeneratedPdf->name = $name;
        $emailGeneratedPdf->phone = $phone;
        $emailGeneratedPdf->email = $from_email;
        $emailGeneratedPdf->room = $room;
        $emailGeneratedPdf->file = $filename;
        $emailGeneratedPdf->date = date("d-m-Y", strtotime($date));
        $emailGeneratedPdf->flag = $flag;
        $emailGeneratedPdf->want_architect = $want_architect;

        $emailGeneratedPdf->save();

        // Mail::to("shakil.shaion@gmail.com")->send(new EmailGenerator($emailGeneratedPdf));
        Mail::to("hello@simonechinaglia.net")->send(new EmailGenerator($emailGeneratedPdf));

        // $email = new \SendGrid\Mail\Mail();
        // $email->setFrom($from_email);
        // $email->setSubject("Ristrutturazionecas");
        // // $email->addTo("info@ristrutturazionecase.com", "");
        // // $email->addTo("hello@simonechinaglia.net", "");
	// $email->addTo("shakil.shaion@gmail.com", "");
        // $email->addContent(
        //     "text/html", new EmailGenerator($emailGeneratedPdf)
        // );
        // $sendgrid = new \SendGrid('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');
        // try {
        //     $response = $sendgrid->send($email);
        //     return response()->json([
        //         "file" => $response
        //     ], 200);
        // } catch (Exception $e) {
        //     echo 'Caught exception: '. $e->getMessage() ."\n";
        // }
    }

    public function getGeneratedPdf($id){
        $pdf = EmailGeneratePdf::where('id', $id)->first();
        return response()->json([
            "pdf" => $pdf
        ], 200);
    }
}

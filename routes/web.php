<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function () {
	Auth::routes();
	Route::get('/', 'AdminController\ProfileController@index')->name('profile.show');
	Route::get('/progetto', 'AdminController\ProfileController@index')->name('profile.show');
	Route::post('/progetto/create', 'AdminController\ProfileController@create')->name('profile.create');
	Route::get('/progetto/aggiorna/{id}', 'AdminController\ProfileController@showEditForm')->name('profile.edit');
	Route::post('/profile/update', 'AdminController\ProfileController@update')->name('profile.update');
	Route::get('/profile/delete/{id}', 'AdminController\ProfileController@delete')->name('profile.delete');

	Route::get('/il-tuo-architetto', 'AdminController\ProfileController@getEmailGeneratedPdf')->name('architect.email.show');

});

Route::get('{any}', function () {
    return view('app');
})->where('any', '.*');

